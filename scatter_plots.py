#!/home/rjm001/.conda/envs/peatlandMLenv/bin/python3.8
''' 
scatter_plots.py
'''
import sys

ifile0 = '/space/hall3/sitestore/eccc/crd/ccrp/crp102/peatlands/peatlandMachineLearning/input/Peatland_training.nc'

rundir=sys.argv[1]   # e.g. output/lgbm_rfepi_corr95_NewPEATLANDP
ifiles=['lgb_cv.nc','lgb.nc']
ofiles=['scatter_cv.png','scatter.png']
suptitles=['Cross-Validation','Model Prediction']

'''
# If blocks is defined, it will be used. Otherwise, it will be obtained from
# the model output directory.

#Ed's new blocks
blocks = [[-145,-115.25,-60,90,"A"],
          [-115.25,-101.25,-60,90,"B"],
          [-101.25,-78.0,-60,90,"C"],
          [-78.0,-12.75,-60,90,"D"],
          [-12.75,16.75,-60,90,"E"],
          [16.75,50.0,-60,90,"F"],
          [50.0,76.75,-60,90,"G"],
          [76.75,112.75,-60,90,"H"],
          [112.75,180,-60,90,"I"],
          [-180, 180, -90, 90, "All Regions"]]
'''
#nrows=3 ; ncols=4
ncols=4

# -----------------------------------------------------------------------------------

import xarray as xr
import numpy as np
from matplotlib import pyplot as plt

# Set parameters for plots.

plt.rc('xtick', labelsize=8)
plt.rc('ytick', labelsize=8)
plt.rc('axes', labelsize=10)

# Get the blocks.

if 'blocks' not in globals(): 
  blocks=np.genfromtxt(rundir+'/blocks.csv',delimiter=',',dtype=['f8','f8','f8','f8','U20'])
  blocks=blocks.tolist()
  blocks.append((-180, 180, -90, 90, "All Regions"))

# Set number of rows based on given number of columns.

nrows=np.ceil(len(blocks)/ncols)

# Open files. 

ds1 = xr.open_dataset(ifile0)  #training data.
fout=open(rundir+'/stats.txt', 'w')

# Loop over CV and full prediction results, compute/save stats and generate plots.

for n in range(2):

  # Output header to stdout and to a file.

  format1 = "{:20} {:>15}" + "{:>15}" * 4

  for m in range(2):
    if m == 0: f=None
    print(file=f)
    print(suptitles[n],file=f) ; 
    print(format1.format('Block','# Cells','MBE','RMSE','r2','r2_metrics'),file=f)
    f=fout

  fig,ax = plt.subplots(facecolor='w',figsize=(16,10))
  ds2 = xr.open_dataset(rundir+'/'+ifiles[n])  # predictions.

  # Restrict values to between 0 and 100.
  with np.errstate(invalid='ignore'):
    ds2['PEATLAND_P'].values[ ds2['PEATLAND_P'].values < 0] = 0
    ds2['PEATLAND_P'].values[ ds2['PEATLAND_P'].values > 100] = 100
  #print(ds2['PEATLAND_P'].min(),ds2['PEATLAND_P'].max(),ds2['PEATLAND_P'].mean())

  # Loop over each of the regions. 
  
  for i in range(len(blocks)):
  
    data1=ds1['PEATLAND_P'].where((blocks[i][0] < ds1.lon) & (ds1.lon < blocks[i][1])
                            & (blocks[i][2] < ds1.lat) & (ds1.lat < blocks[i][3]), drop=True).to_masked_array()
    data2=ds2['PEATLAND_P'].where((blocks[i][0] < ds2.lon) & (ds2.lon < blocks[i][1])
                            & (blocks[i][2] < ds2.lat) & (ds2.lat < blocks[i][3]), drop=True).to_masked_array()
    ds2.close()
  
    # Compute error and rmse.
  
    err=data2-data1  # predictions - training data.
    rmse=np.sqrt(((err)**2).mean())
  
    # Apply the combined masks of data1/data2 from the computation of err to each dataset.
  
    data1.mask=err.mask
    data2.mask=err.mask
    x=data1.compressed()  # training 
    y=data2.compressed()  # predictions
    
    # Get polynomial coefficients of best fit line.
    # Note: np.polyfit crashes if all values of x is zero.
  
    if x.max() > 0:

      degree=1
      coeffs = np.polyfit(x,y,degree)
  
      # Get function for the line.
  
      p = np.poly1d(coeffs)
  
      # Compute r2.
  
      ssreg = np.sum((p(x)-y.mean())**2)
      sstot = np.sum((y - y.mean())**2)
      r2 = ssreg / sstot
  
      # Compute r2 as done by scikit-learn metrics.
  
      ss_res=np.sum(err**2)  
      ss_tot=np.sum((x-x.mean())**2)  #training.
      r2_metrics=1-ss_res/ss_tot

    # -----------------------------------------------------------------------------------

    # Print out the results.

    format1 = "{:20} {:>15}" + "{:15.4f}" * 2
    format2 = "{:15.4f}" * 2

    for m in range(2):
      if m == 0: f=None
      print(format1.format(blocks[i][4],str(x.size),round(err.mean(),4),round(rmse,4)),end='',file=f)
      if x.max() > 0: 
        print(format2.format(round(r2,4),round(r2_metrics,4)),file=f)
      else:
        print(file=f)
      f=fout
    
    # Generate the scatter plot.
  
    ax=plt.subplot(nrows,ncols,i+1)
    sc=ax.scatter(x,y,c='b',alpha=0.25,s=5,edgecolor='')
    
    # Plot the best fit line. Skip if all values of x is zero.
    
    axmin=0 ; axmax=100
    xx=np.array([axmin,axmax])
    if x.max() > 0: ax.plot(xx,p(xx),'r')
  
    # Plot diagonal. 
  
    ax.plot([axmin,axmax],[axmin,axmax],'k')
    
    # Set the axes limits.
    
    ax.set_xlim(axmin,axmax)
    ax.set_ylim(axmin,axmax)
    
    # Add titles.
    
    plt.title(blocks[i][4],fontsize=12,weight='bold')
  
    if not (i % ncols): plt.ylabel('Predicted Values')
    if i+1>(nrows-1)*ncols: plt.xlabel('Actual Values')
    
    # Ensure equal aspect ratio for scatter plot. 
    
    ax.set_aspect('equal')
    
    # Add text annotations. Numbers are rounded/formatted to 2 decimal places. Skip r2 scores if all values of x are zero.
    
    text=['N='+str(x.size),'MBE='+"{:.2f}".format(round(err.mean(),2)),'RMSE='+"{:.2f}".format(round(rmse,2))]
    if x.max() > 0: text.extend(['r$^{\/2}$='+"{:.2f}".format(round(r2,2)),'r$^{\/2}$(metrics)='+"{:.2f}".format(round(r2_metrics,2))])

    halign='left' ; valign='top'
    xx,yy=0.05,0.95
  
    legend_fontsize=np.min([int(10/len(blocks)*10),10])
    options={'xycoords':'axes fraction','horizontalalignment':halign,'verticalalignment':valign,'fontsize':legend_fontsize,'weight':'bold','color':'r'}
  
    for i in range(len(text)):
      ax.annotate(text[i],xy=(xx,yy-i*0.05),**options)
  
  # Make room for suptitle and save plot.
  
  fig.suptitle(suptitles[n],fontsize=14,weight='bold')
  plt.tight_layout(rect=[0,0,1,0.95])
  plt.savefig(rundir+'/'+ofiles[n],orientation='landscape')
