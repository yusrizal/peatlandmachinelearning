RUNNAME="PeatlandsML"
presdir=$PWD
outdir=$PWD/$1
echo $outdir
usage()
{
	    echo "
Usage: $0 ML_output_directory 
"
exit
}

if [ $# -lt 1 ]; then
    usage
fi

cat << endjob > $RUNNAME.job
#!bin/bash
#PBS -S /bin/bash
#PBS -N $RUNNAME
#PBS -l walltime=6:00:00
#PBS -q development
#PBS -j oe
#PBS -o /space/hall3/sitestore/eccc/crd/ccrn/users/rjm001/peatlandML/peatlandMachineLearning/$(date +%F_%H-%M-%S)_stdout.out
#PBS -l select=1:ncpus=40:mem=160G:res_image=eccc/eccc_all_ppp_ubuntu-18.04-amd64_latest,place=free
#PBS -m abe -M Joe.Melton@ec.gc.ca

cd /space/hall3/sitestore/eccc/crd/ccrn/users/rjm001/peatlandML/peatlandMachineLearning

source ~rjm001/.conda_init
conda activate peatlandMLenv

python model.py
python scatter_plots.py $outdir
cp /space/hall3/sitestore/eccc/crd/ccrn/users/rjm001/peatlandML/peatlandMachineLearning/model.py ${outdir}

endjob


chmod a+x $RUNNAME.job

jobsub -c eccc-ppp3 $RUNNAME.job
rm -f $RUNNAME.job


