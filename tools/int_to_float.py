import numpy as np
import xarray as xr
import sys

inf='input/PEATLAND_P.nc'

inf_xr = xr.open_dataset(inf)

print(inf_xr['PEATLAND_P'].attrs)

inf_xr['PEATLAND_P'] = inf_xr['PEATLAND_P'].astype(np.float32)


inf_xr.to_netcdf('input/PEATLAND_P_2.nc')
